<?php

namespace Tests\Unit;

use App\Disc;
use App\Tower;
use Exception;
use Tests\TestCase;

class TowerTest extends TestCase
{
    public function testTowerClassConstruction()
    {
        try {
            $isFull = rand(0, 1);
            $tower = new Tower($isFull);
            $tower1 = new Tower(!$isFull);
            $this->assertEquals($isFull ? 7 : 0, $tower->size());
            $this->assertEquals($isFull ? 0 : 7, $tower1->size());
        } catch (Exception $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }

    public function testTowerIsFullFunction()
    {
        try {
            $tower = new Tower();
            $this->assertFalse($tower->isFull());
            $tower = new Tower(true);
            $this->assertTrue($tower->isFull());
        } catch (Exception $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }

    public function testTowerPushFunctionExceptionOnDisc()
    {
        $tower = new Tower(true);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Invalid disc size");
        $tower->push(new Disc(0));
    }

    public function testTowerPushFunctionException()
    {
        $tower = new Tower(true);
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Stack is full, can't push");
        $tower->push(new Disc(1));
    }

    public function testTowerIsEmptyFunction()
    {
        try {
            $tower = new Tower(true);
            $this->assertFalse($tower->isEmpty());
            $tower = new Tower();
            $this->assertTrue($tower->isEmpty());
        } catch (Exception $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }

    public function testTowerPopFunction()
    {
        try {
            $tower = new Tower(true);
            for ($i = 7; $i > 0; $i--) {
                $tower->pop();
                $this->assertEquals($i - 1, $tower->size());
            }
        } catch (Exception $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }

    public function testTowerPopFunctionException()
    {
        $tower = new Tower();
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Stack is empty, can't pop");
        $tower->pop();
    }

    public function testTowerPeekFunction()
    {
        try {
            $tower = new Tower(true);
            for ($i = 7; $i > 0; $i--) {
                $last = $tower->peek();
                $this->assertSame($last, $tower->pop());
            }
        } catch (Exception $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }

    public function testTowerPeekFunctionException()
    {
        $tower = new Tower();
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Can't peek in empty stack");
        $tower->peek();
    }
}
