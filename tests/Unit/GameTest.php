<?php

namespace Tests\Unit;

use App\Game;
use Exception;
use Tests\TestCase;

class GameTest extends TestCase
{
    public function testGameClassConstruction()
    {
        $game = new Game();
        $game->init();
        $tower1 = $game->getTower(0);
        $tower2 = $game->getTower(1);
        $tower3 = $game->getTower(2);
        $this->assertEquals(7, $tower1->size());
        $this->assertEquals(0, $tower2->size());
        $this->assertEquals(0, $tower3->size());
        $this->assertFalse($game->isOver());
    }
}
