<?php

namespace Tests\Unit;

use App\Disc;
use Exception;
use Tests\TestCase;

class DiscTest extends TestCase
{
    public function testDiscClassConstruction()
    {
            $size = rand(1,7);
            $disc = new Disc(8);
            $this->assertSame($size, $disc->size());
    }

    public function testDiscClassConstructionFailedInvalidSize()
    {
        $this->expectException(Exception::class);
        $this->expectExceptionMessage("Invalid disc size");
        $size = rand(1,7) + 7;
        new Disc($size);
    }

    public function testCompareDiscSize()
    {
        try {
            $disc1 = new Disc(1);
            $disc2 = new Disc(2);
            $disc3 = new Disc(3);

            $this->assertTrue($disc2->isGreaterThan($disc1));
            $this->assertTrue($disc3->isGreaterThan($disc1));
            $this->assertFalse($disc2->isGreaterThan($disc3));
            $this->assertFalse($disc1->isGreaterThan($disc3));
        } catch (Exception $e) {
            $this->markTestSkipped($e->getMessage());
        }
    }
}
