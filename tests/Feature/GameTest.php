<?php

namespace Tests\Feature;

use App\Game;
use Exception;
use Tests\TestCase;

class GameTest extends TestCase
{
    public function testMoveDisc()
    {
        $game = new Game();
        $game->init();
        $tower1 = $game->getTower(0);
        $tower2 = $game->getTower(2);
        $game->move(1, 3);
        $this->assertEquals(6, $tower1->size());
        $this->assertEquals(1, $tower2->size());
    }

    public function testMoveDiscInvalidSourceTower()
    {
        $game = new Game();
        $game->init();
        $this->expectException(Exception::class);
        $game->move(4, 3);
        $this->expectException(Exception::class);
        $game->move(0, 3);
    }

    public function testMoveDiscInvalidDestinationTower()
    {
        $game = new Game();
        $game->init();
        $this->expectException(Exception::class);
        $game->move(1, 4);
        $this->expectException(Exception::class);
        $game->move(1, 0);
    }

    public function testMoveDiscFromEmptyTower()
    {
        $game = new Game();
        $game->init();
        $this->expectException(Exception::class);
        $game->move(3, 1);
    }

    public function testMove()
    {
        $game = new Game();
        $game->init();

        $game->move(1, 2);
        $this->assertEquals(6, $game->getTower(0)->size());
        $this->assertEquals(1, $game->getTower(1)->size());
        $this->assertEquals(0, $game->getTower(2)->size());
        $this->assertEquals(1, $game->getTurn());

        $game->move(2, 1);
        $this->assertEquals(7, $game->getTower(0)->size());
        $this->assertEquals(0, $game->getTower(1)->size());
        $this->assertEquals(2, $game->getTurn());

        $game->move(1, 3);
        $this->assertEquals(0, $game->getTower(1)->size());
        $this->assertEquals(1, $game->getTower(2)->size());
        $this->assertEquals(3, $game->getTurn());
        $this->assertFalse($game->isOver());
    }

    public function testWinGame()
    {
        $game = new Game();
        $game->init();

        $this->assertFalse($game->isOver());
        $this->resolveGame($game, 7, 1, 3, 2);
        $this->assertTrue($game->isOver());
    }

    private function resolveGame(&$game, $n, $from, $to, $aux) {
        if ($n > 0) {
            $this->resolveGame($game, $n - 1, $from, $aux, $to);
            $game->move($from, $to);
            $this->resolveGame($game, $n - 1, $aux, $to, $from);
        }
    }

    public function testInvalidMove()
    {
        $game = new Game();
        $game->init();

        $this->expectException(Exception::class);
        $this->expectExceptionMessage('Movement not allowed');
        $game->move(1, 2);
        $game->move(1, 2);
    }
}
