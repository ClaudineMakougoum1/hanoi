<?php

namespace Tests\Feature;

use App\Disc;
use App\Tower;
use Exception;
use Tests\TestCase;

class DrawTest extends TestCase
{
    public function testDrawDisc()
    {
        for ($i = 1; $i <= 7; $i++) {
            $disc = new Disc($i);
            ob_start();
            $disc->draw();
            $content = ob_get_clean();
            $this->assertSame('<div class="disc"><div class="disc-'.$i.'"></div></div>', $content);
        }
    }

    public function testDrawEmptyTower()
    {
        $tower = new Tower();
        ob_start();
        $tower->draw();
        $content = ob_get_clean();
        $this->assertSame('<div class="tower"></div>', $content);
    }

    public function testDrawFullTower()
    {
        $tower = new Tower(true);
        ob_start();
        $tower->draw();
        $content = ob_get_clean();
        $view = '';
        for ($i = 7; $i > 0; $i--) {
            $view.= '<div class="disc"><div class="disc-' . $i . '"></div></div>';
        }

        $this->assertSame('<div class="tower">' . $view . '</div>', $content);
    }
}
