<?php

namespace Tests\Execution;

use App\Game;
use App\GameException;
use Tests\TestCase;

class AppTest extends TestCase
{
    public function testInvalidFromNumberException()
    {
        $game = new Game();
        $game->init();
        $this->expectExceptionMessage('Invalid "from" number');
        $game->move(0, 2);
    }

    public function testInvalidFromNumberCase2Exception()
    {
        $game = new Game();
        $game->init();
        $this->expectExceptionMessage('Invalid "to" number');
        $game->move(1, 4);
    }

    public function testInvalidFromNumberCase3Exception()
    {
        $game = new Game();
        $game->init();
        $this->expectExceptionMessage('Invalid "from" number');
        $game->move(0, 4);
    }

    public function testInvalidMoveException()
    {
        $game = new Game();
        $this->expectException(GameException::class);
        $this->expectExceptionMessage('Source tower is empty');
        $game->move(1, 2);
    }
}
